--use AdventureWorks2016_EXT;
-- after trigger
go
create trigger LocationInsert	
on Production.Location
After Insert
as
begin
	set Nocount on;  --performans i�in
		select 'New location created';
	set Nocount off;
end;

DROP TRIGGER Production.LocationInsert


INSERT INTO Production.Location(Name,CostRate,Availability,ModifiedDate)
			VALUES('IZMIR',0.00,0.00,GETDATE());

TRUNCATE TABLE ProductLog;
-------------------------------

go
CREATE TRIGGER trg_ProductLogger
on Production.Product  -- trigger bu k�s�mdaki tabloda tan�mlan�r
after insert
as 
begin
	insert into ProductLog
	select ProductID,Name,ProductNumber,ListPrice from inserted;
end;

insert into Production.Product(Name,ProductNumber,MakeFlag,FinishedGoodsFlag,SafetyStockLevel,ReorderPoint,
						StandardCost,ListPrice,DaysToManufacture,SellStartDate,rowguid,ModifiedDate)
						VALUES('Test i�in yaz�ld�','TT-3687',1,0,500,700,0,0,3,GETDATE(),NEWID(),GETDATE());
----------------------------------
go
create trigger trg_GetProduct
on Production.Product
after insert
as
begin
	select PL.ProductID,PL.Name,PL.ProductNumber
	from Production.Product as PL
	inner join inserted as I
	on PL.ProductID = I.ProductID;
end; 

insert into Production.Product(Name,ProductNumber,MakeFlag,FinishedGoodsFlag,SafetyStockLevel,ReorderPoint,
						StandardCost,ListPrice,DaysToManufacture,SellStartDate,rowguid,ModifiedDate)
						VALUES('Test i�in yaz�ld�yd�','TU-3687',1,0,500,700,0,0,3,GETDATE(),NEWID(),GETDATE());
--- delete trigger
go
create trigger trg_KullaniciSil
on Kullanicilar
after delete
as
begin	
	select deleted.KullaniciAd +' kullanici ad�na ve ' +
		   deleted.Email +' email adresine sahip kullan�c� silindi'
		   from deleted;
end;

delete from Kullanicilar where Kullanici_ID=3

--update trigger
go
create trigger trg_UrunGuncellemetarihiniguncelle
on Production.Product
after update
as
begin
	update Production.Product
	set ModifiedDate=getdate()
	where ProductID=(select ProductID from inserted);
end;

select * from Production.Product where ProductID=999

update Production.Product
set Name= 'Road-750 Red, 15'
where ProductID=999
---------
go
create trigger trg_UrunGuncelleLog
on Production.Product
after update
as
begin
	declare @ProductID INT,@Name VARCHAR(50),@ProductNumber NVARCHAR(25),@ListPrice Money,@ModifiedDate Datetime;

	select @ProductID = i.ProductID, @Name=i.Name,@ProductNumber=i.ProductNumber,
			@ListPrice = i.ListPrice,@ModifiedDate = i.ModifiedDate
			from inserted as i;
	insert into UrunGuncellemeLog
	Values(@ProductID,@Name,@ProductNumber,@ListPrice,@ModifiedDate);
end;

update Production.Product
set Name= 'Road-750 Blue, 15'
where ProductID=999
--------------DELETE-- ==>> IsActive=true/false
go
create trigger trg_KullaniciPasifYap
on Kullanicilar
Instead of delete
as 
begin
	update Kullanicilar
	set isActive=0
	where Kullanici_ID=(select Kullanici_ID from deleted);
end;

delete from Kullanicilar where KullaniciAd = 'abc'