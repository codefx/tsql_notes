--use NORTHWND;

--INSERT 
create procedure CategoryAdd
@Name nvarchar(50),
@Description nvarchar(200)
as
declare @trimmed nvarchar(50)=ltrim(rtrim(@Name))
if(exists(select * from Categories where CategoryName=@trimmed))
		print 'boyle bir kategori zaten var'
else
begin
insert Categories (CategoryName,Description) values(@trimmed,@Description)
end;

--Delete
go
create proc CategoryDel
@CatID int
as
delete from Categories where CategoryID=@CatID;

--List
go
create proc prc_CategoryList
as
select * from Categories

GO
--UPDATE
create proc CategoryUpdate
@CatID int,
@CatName nvarchar(15),
@Description  nvarchar(200)
as
update Categories 
		set CategoryName=@CatName, Description=@Description 
		where CategoryID=@CatID;